<?php 

require_once('../../../../../wp-load.php');

global $wpdb;

if(!is_user_logged_in()){
	wp_redirect( home_url() );
	exit;
}

$arquivo = 'fale_conosco' . date("d-m-y") . '_'.date("h-i").'.xls';

$contatos = $wpdb->get_results("SELECT * FROM ".$wpdb->base_prefix."form_faleconosco ORDER BY id DESC");

$fields = array(
	"Nome" => "nome",
	"Email" => "email",
	"Telefone" => "telefone",
	"Mensagem" => "mensagem",
);


$html = '';
$html .= '<table border="1">';
	$html .= '<tr>';
		foreach ($fields as $key => $value) {
			$html .= "<td><b>".$key."</b></td>";
		}
	$html .= '</tr>';
	if ($contatos):
		foreach($contatos as $contato):
			$html .= '<tr>';
				foreach ($fields as $key => $value) {
					$html .= '<td valign="top">';
					$html .= ($contato->$value) ? $contato->$value : '-';
					$html .= '</td>';
				}
			$html .= '</tr>';
		endforeach;
	endif;
$html .= '</table>';

// Configurações header para forçar o download
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header ("Cache-Control: no-cache, must-revalidate");
header ("Pragma: no-cache");
header ("Content-type: application/x-msexcel");
header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
header ("Content-Description: PHP Generated Data" );

echo utf8_decode($html);

exit;