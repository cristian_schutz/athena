<?php

global $wpdb, $table_contato;
$table_contato = $wpdb->base_prefix."form_faleconosco";

if ($wpdb->get_var("SHOW TABLES LIKE '".$table_contato."'") != $table_contato ):
	$wpdb->query("CREATE TABLE ".$table_contato." ( id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, nome VARCHAR(255), email VARCHAR(255), telefone VARCHAR(255), mensagem TEXT, created TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP )");
endif;

add_action('admin_menu', 'faleconosco_create_menu');
function faleconosco_create_menu() {
	add_menu_page('Fale Conosco', 'Fale Conosco', 'read', basename(__FILE__), 'faleconosco_settings_page', 'dashicons-phone', 505);
}

function faleconosco_settings_page() {
	global $wpdb;
	$table_contato = $wpdb->base_prefix."form_faleconosco";
?>
	<div class="wrap">
		<div class="container">
			<h1>Fale Conosco</h1>
			<hr>
			<?php
				if ( $_GET['id'] ):
					$delete = $wpdb->query( "DELETE FROM ".$table_contato." WHERE id = '". $_GET['id'] ."'" );
					echo '<div id="message" class="updated"><p><strong>Contato apagado com sucesso.</strong></p></div>';
				endif;
				$contatos = $wpdb->get_results("SELECT * FROM ".$table_contato." ORDER BY id DESC LIMIT 100");
			?>
			<table class="widefat" id="contato">
				<thead>
					<tr>
						<th class="manage-column">#</th>
						<th class="manage-column">Nome</th>
						<th class="manage-column">Email</th>
						<th class="manage-column">Telefone</th>
						<th class="manage-column">Mensagem</th>
						<th class="manage-column">-</th>
						<th class="manage-column">-</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if($contatos): 
							foreach($contatos as $contato):
								$other = json_decode($contato->other);
					?>
						<tr>
							<td><?php echo $contato->id; ?></td>
							<td><?php echo $contato->nome; ?></td>
							<td><?php echo $contato->email; ?></td>
							<td><?php echo $contato->telefone; ?></td>
							<td><?php echo $contato->mensagem; ?></td>
							<td>
								<a href="<?php echo get_bloginfo("url"). "/wp-admin/admin.php?page=" . basename(__FILE__); ?>&id=<?php echo $contato->id; ?>" class="btn btn-danger btn-sm">EXCLUIR</a>
							</td>
						</tr>
					<?php 
							endforeach;
						endif;
					?>
				</tbody>
			</table>

			<hr>

			<p>
				<a href="<?php bloginfo("template_url"); ?>/inc/form-fale-conosco/download.php" target="_blank" class="button">
					Download dos Contatos
				</a>
			</p>

		</div>
	</div>
	<?php
}

add_action( 'wp_ajax_nopriv_method_faleconosco', 'method_faleconosco' );
add_action( 'wp_ajax_method_faleconosco', 'method_faleconosco' );
function method_faleconosco(){
	
	global $wpdb;
	$table_contato = $wpdb->base_prefix."form_faleconosco";

	$data = array(
		"nome" => $_POST["nome"],
		"email" => $_POST["email"],
		"telefone" => $_POST["telefone"],
		"mensagem" => $_POST["mensagem"],
	);
	$wpdb->insert( $table_contato, $data );

	#
	# Envia Email
	#
	$data_email = array(
		"Assunto" => $_POST["assunto"],
		"Nome" => $_POST["nome"],
		"Email" => $_POST["email"],
		"Telefone" => $_POST["telefone"],
		"Mensagem" => $_POST["mensagem"],
	);

	$from = get_bloginfo("admin_email");

	$destinatario = get_bloginfo("admin_email");

	$assunto = "[". get_bloginfo("name") ."] ".$_POST["assunto"];

	$headers = "MIME-Version: 1.1\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\n";
	$headers .= "From: ".$from ."\n";

	$body = '<table>';
	foreach ($data_email as $key => $value) {
		if ($value) {
			$body .= '<tr><td bgcolor="#f4f5f6"><font face="Arial, Tahoma, Sans-serif" size="3" color="#737c8f">'.$key.'</font>';
			$body .= '</td><td bgcolor="#e9ebed"><font face="Arial, Tahoma, Sans-serif" size="3" color="#737c8f">'.$value.'</font></td></tr>';
		}
	}
	$body .= '</table>';

	if (wp_mail( $destinatario, $assunto, $body, $headers)):
		$return = array(
			"status" => "success",
			"class" => "alert-success",
			"messenge" => '<p><strong>'.$_POST["nome"].'</strong>, seu email foi enviado com sucesso. Em breve entraremos em contato pelos dados: <br><strong>'.$_POST["email"] .'</strong> | <strong>'. $_POST["telefone"] .'</strong></p>'
		);
	else:
		$return = array(
			"class" => "alert-danger",
			"messenge" => "Estamos passando por manutenção, tente novamente mais tarde."
		);
	endif;

	echo json_encode($return);

	wp_die();
}
