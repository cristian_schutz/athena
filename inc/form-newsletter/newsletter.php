<?php

global $wpdb, $table_contato;

$table_contato = $wpdb->base_prefix."form_newsletter";

if ($wpdb->get_var("SHOW TABLES LIKE '".$table_contato."'") != $table_contato ):
	$wpdb->query("CREATE TABLE ".$table_contato." ( id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, email VARCHAR(255), created TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP )");
endif;

add_action('admin_menu', 'newletter_create_menu');
function newletter_create_menu() {
	add_menu_page('Newsletter', 'Newsletter', 'read', basename(__FILE__), 'newletter_settings_page', 'dashicons-email-alt', 505);
}

function newletter_settings_page() {
	global $wpdb, $table_contato;
	$user = $user ? new WP_User( $user ) : wp_get_current_user();
	$userRoles = $user->roles ? $user->roles : false;
?>
	<div class="wrap">
		<div class="container">
			<h1>Newsletter</h1>
			<hr>
			<?php
				if ( $_GET['id'] ):
					$delete = $wpdb->query( "DELETE FROM ".$table_contato." WHERE id = '". $_GET['id'] ."'" );
					echo '<div id="message" class="updated"><p><strong>Contato apagado com sucesso.</strong></p></div>';
				endif;
				$contatos = $wpdb->get_results("SELECT * FROM ".$table_contato." ORDER BY id DESC LIMIT 100");
			?>
			<table class="widefat" id="contato">
				<thead>
					<tr>
						<th class="manage-column">#</th>
						<th class="manage-column">Email</th>
						<th class="manage-column">-</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if($contatos): 
							foreach($contatos as $contato):
								$other = json_decode($contato->other);
					?>
						<tr>
							<td><?php echo $contato->id; ?></td>
							<td><?php echo $contato->email; ?></td>
							<td>
								<a href="<?php echo get_bloginfo("url"). "/wp-admin/admin.php?page=" . basename(__FILE__); ?>&id=<?php echo $contato->id; ?>" class="btn btn-danger btn-sm">EXCLUIR</a>
							</td>
						</tr>
					<?php 
							endforeach;
						endif;
					?>
				</tbody>
			</table>

			<hr>

			<p>
				<a href="<?php bloginfo("template_url"); ?>/inc/form-newsletter/download.php" target="_blank" class="button">
					Download dos Contatos
				</a>
			</p>

		</div>
	</div>
	<?php
}

add_action( 'wp_ajax_nopriv_method_newsletter', 'method_newsletter' );
add_action( 'wp_ajax_method_newsletter', 'method_newsletter' );
function method_newsletter(){

	global $wpdb, $table_contato;

	$data = array(
		"email" => $_POST["email"]
	);
	$wpdb->insert( $table_contato, $data );

	#
	# Envia Email
	#
	$data_email = array(
		"Email" => $_POST["email"]
	);

	$from = get_bloginfo("admin_email");

	$destinatario = get_bloginfo("admin_email");

	$assunto = "[". get_bloginfo("name") ."] Newsletter";

	$headers = "MIME-Version: 1.1\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\n";
	$headers .= "From: ".$from ."\n";

	$body = '<table>';
	foreach ($data_email as $key => $value) {
		if ($value) {
			$body .= '<tr><td bgcolor="#f4f5f6"><font face="Arial, Tahoma, Sans-serif" size="3" color="#737c8f">'.$key.'</font>';
			$body .= '</td><td bgcolor="#e9ebed"><font face="Arial, Tahoma, Sans-serif" size="3" color="#737c8f">'.$value.'</font></td></tr>';
		}
	}
	$body .= '</table>';

	if (wp_mail( $destinatario, $assunto, $body, $headers)):
		$return = array(
			"status" => "success",
			"class" => "alert-success",
			"messenge" => 'Email cadastrado com sucesso.'
		);
	else:
		$return = array(
			"class" => "alert-danger",
			"messenge" => "Estamos passando por manutenção, tente novamente mais tarde."
		);
	endif;

	echo json_encode($return);

	wp_die();
}
