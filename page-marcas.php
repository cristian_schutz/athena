<?php /* Template Name: Marcas */ ?>
<?php get_header(); ?>
    <section class="marcas-header" style="background-image: url(<?php bloginfo('template_url'); ?>/_assets/images/marcas.jpg);">
        <div class="container">
            <div class="marcas-header--highlight">
                <h6 style="color: #fff;">
                    Parceria inovadora<br class="d-none d-md-block">para ir <br class="d-none d-md-block">mais longe.
                </h6>
            </div>
            <div class="marcas-header--text">
                <p>A Athena já estabeleceu parcerias com empresas bem <br class="d-none d-md-block">
                sucedidas e reconhecidas e, juntos, vamos oferecer as <br class="d-none d-md-block">
                melhores soluções de saúde humanizada.</p>
            </div>
        </div>
    </section>

    <section class="marcas-items">
        <div class="main-heading text-center" style="margin-top: -240px;">
            <h5 class="text-white">Operadoras</h5>
        </div>
        <div class="marcas-box alt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-6 d-flex justify-content-center">
                       <a href="https://www.humanasaude.com.br/" target="_blank"> <img src="<?php bloginfo('template_url'); ?>/_assets/images/marcas-humana.png" alt=""></a>
                    </div>
                    <div class="col-lg-4 col-6 d-flex justify-content-center">
                      <a href="https://www.medplan.com.br/" target="_blank"> <img src="<?php bloginfo('template_url'); ?>/_assets/images/marcas-medplan.jpg" style="width: 270px;" alt=""></a>
                    </div>
                    <div class="col-lg-4 col-6 d-flex justify-content-center">
                       <a href="https://www.samp.com.br/es/"><img src="<?php bloginfo('template_url'); ?>/_assets/images/marcas-samp.png" alt=""></a> 
                    </div>
                </div>
            </div>
        </div>

        <div class="main-heading text-center">
            <h5 class="text-white">Clínicas e prontos atendimentos</h5>
        </div>
        <div class="marcas-box">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-6 d-flex justify-content-center">
                       <a href="#" target="_blank"> <img src="<?php bloginfo('template_url'); ?>/_assets/images/marcas-humana.png" alt=""></a>
                    </div>
                    <div class="col-lg-4 col-6 d-flex justify-content-center">
                       <a href="http://imagemplena.com.br/home/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/marcas-imgplena.png" alt=""></a>
                    </div>
                    <div class="col-lg-4 col-6 d-flex justify-content-center">
                      <a href="https://medimagem.com.br/">  <img src="<?php bloginfo('template_url'); ?>/_assets/images/marcas-med.png" alt=""></a>
                    </div>
                    <div class="col-lg-4 col-6 d-flex justify-content-center">
                       <a href="https://www.medplan.com.br/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/marcas-medplan.jpg" style="width: 270px;" alt=""></a>
                    </div>
                    <div class="col-lg-4 col-6 d-flex justify-content-center">
                      <a href="http://www.oncomedica.com.br/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/marcas-oncomedica.png" alt=""></a>
                    </div>
                    <div class="col-lg-4 col-6 d-flex justify-content-center">
                       <a href="https://www.samp.com.br/es/" target="_blank"> <img src="<?php bloginfo('template_url'); ?>/_assets/images/marcas-samp.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>


        <div class="main-heading text-center">
            <h5 class="text-white">Hospitais</h5>
        </div>
        <div class="marcas-box alt">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-6 d-flex justify-content-center">
                     <a href="http://hsmaria.com.br/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/marcas-santamaria.png" alt=""></a>
                    </div>
                    <div class="col-lg-4 col-6 d-flex justify-content-center">
                      <a href="http://medimagem.com.br/saopedro" target="_blank">  <img src="<?php bloginfo('template_url'); ?>/_assets/images/marcas-saopedro.png" alt=""></a>
                    </div>
                    <div class="col-lg-4 col-6 d-flex justify-content-center">
                     <a href="http://medimagem.com.br/hospitalvitoria" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/marcas-vitoria.png" alt=""></a>
                    </div>
                    
                    <div class="col-lg-4 col-6 d-flex justify-content-center">
                     <a href="https://medimagem.com.br/prontomed-adulto" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/marcas-prontomed.png" alt=""></a> 
                    </div>

                    <div class="col-lg-4 col-6 d-flex justify-content-center">
                     <a href="https://medimagem.com.br/category/prontomed-infantil" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/marcas-prontomed-infanto.jpg" alt=""></a> 
                    </div>

                    <div class="col-lg-4 col-6 d-flex justify-content-center">
                      <a href="http://www.vitoriaaparthospital.com.br/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/marcas-vitoriasapart.png" alt=""></a>  
                    </div>
                </div>
            </div>
        </div>






    </section>

    <?php get_template_part('section','noticias'); ?>
<?php get_footer(); ?>