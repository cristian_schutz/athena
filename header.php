            <!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo wp_title(); ?></title>
    
    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

    <div id="loader"></div>
    <div class="main">
        <header>
            <div class="header">
                <div class="container">
                    <a href="<?php bloginfo('url'); ?>" class="logo">
                        <img src="<?php bloginfo('template_url') ?>/_assets/images/logo.png" alt="Logo Athena">
                    </a>
                    <nav>
                        <a <?php echo is_page('A Athena') ? 'class="active"' : '';?> href="<?php bloginfo('url'); ?>/a-athena">A ATHENA</a>
                        <a <?php echo is_page('Marcas do Portfólio') ? 'class="active"' : '';?> href="<?php bloginfo('url'); ?>/marcas-do-portfolio">NOSSAS MARCAS</a>
                        <a <?php echo (is_page('Notícias') || is_single()) ? 'class="active"' : '';?> href="<?php bloginfo('url'); ?>/noticias">NOTÍCIAS</a>
                        <a <?php echo (is_page('Fale conosco') || is_page('Fale com a gente') || is_page('Contato')) ? ' class="active" ' : '';?> href="<?php bloginfo('url'); ?>/fale-com-a-gente">FALE CONOSCO</a>
                        <a <?php echo (is_page('Fale conosco') || is_page('Fale com a gente') || is_page('Contato')) ? ' class="active" ' : '';?> href="<?php bloginfo('url'); ?>/trabalhe-na-athena">TRABALHE NA ATHENA</a>
                    </nav>
                    <a href="https://www.linkedin.com/company/athenasaude/about/" target="_blank" class="header--linkedin">
                        <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="linkedin-in" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-linkedin-in fa-w-14"><path fill="currentColor" d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z" class=""></path></svg>
                    </a>
                </div>
            </div>

            <div class="header-mobile">
                <a href="<?php bloginfo('url'); ?>" class="logo">
                    <img src="<?php bloginfo('template_url') ?>/_assets/images/logo.png" alt="Logo Athena">
                </a>
                <button class="header-mobile--button">
                    <div></div>
                    <span>MENU</span>
                </button>
            </div>

            <div class="header-mobile-nav">
                <a <?php echo is_page('A Athena') ? 'class="active"' : '';?> href="<?php bloginfo('url'); ?>/a-athena">A ATHENA</a>
                <a <?php echo is_page('Marcas do Portfólio') ? 'class="active"' : '';?> href="<?php bloginfo('url'); ?>/marcas-do-portfolio">NOSSAS MARCAS</a>
                <a <?php echo (is_page('Notícias') || is_single()) ? 'class="active"' : '';?> href="<?php bloginfo('url'); ?>/noticias">NOTÍCIAS</a>
                <a <?php echo is_page('Fale com a gente') ? 'class="active"' : '';?> href="<?php bloginfo('url'); ?>/fale-com-a-gente">FALE CONOSCO</a>
                <a href="<?php bloginfo('url'); ?>/trabalhe-na-athena">TRABALHE NA ATHENA</a>
            </div>
        </header>