<?php /* Template Name: Home */ ?>
<?php get_header(); ?>

    <section class="home-slider">
        <?php if( have_rows('section_slider') ): ?>
            <div class="owl-carousel">
                <?php while( have_rows('section_slider') ): the_row(); ?>
                    <div class="home-slider--item" style="background-image: url(<?php the_sub_field('image'); ?>);">
                        <div class="home-slider--text">
                            <h6><?php the_sub_field('heading'); ?></h6>
                            <h4><?php the_sub_field('highlight'); ?></h4>
                            <p><?php the_sub_field('text'); ?></p>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
        <a href="#numeros" class="main-arrow-down js-scrollto">
            <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-chevron-down fa-w-14"><path fill="currentColor" d="M441.9 167.3l-19.8-19.8c-4.7-4.7-12.3-4.7-17 0L224 328.2 42.9 147.5c-4.7-4.7-12.3-4.7-17 0L6.1 167.3c-4.7 4.7-4.7 12.3 0 17l209.4 209.4c4.7 4.7 12.3 4.7 17 0l209.4-209.4c4.7-4.7 4.7-12.3 0-17z" class=""></path></svg>
        </a>
        <div class="container position-relative">
            <a href="#novidades" class="home-slider--novidades js-scrollto">
                Confira as novidades<br> 
                sobre as marcas da<br> 
                Athena Saúde
                <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="chevron-double-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-chevron-double-down fa-w-14"><path fill="currentColor" d="M441.9 89.7L232.5 299.1c-4.7 4.7-12.3 4.7-17 0L6.1 89.7c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L224 233.6 405.1 52.9c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17zm0 143l-19.8-19.8c-4.7-4.7-12.3-4.7-17 0L224 393.6 42.9 212.9c-4.7-4.7-12.3-4.7-17 0L6.1 232.7c-4.7 4.7-4.7 12.3 0 17l209.4 209.4c4.7 4.7 12.3 4.7 17 0l209.4-209.4c4.7-4.7 4.7-12.3 0-17z" class=""></path></svg>
            </a>
        </div>
    </section>
    <?php $numbers = get_field('section_numbers'); ?>

    <?php if($numbers): ?>
        <section id="numeros" class="home-numbers">
            <div class="container d-flex align-content-center align-items-center justify-content-between flex-md-row flex-column">
                <?php 
                    if($numbers['numbers']) :
                        foreach($numbers['numbers'] as $number):
                            $i++;
                ?>
                            <div class="home-numbers--item">
                                <p>
                                    <span style="min-height: 30px;"><?php echo $number['heading'];?></span>
                                    <strong><?php echo $number['text'];?></strong> 
                                    <span style="min-height: 200px;"><?php echo $number['desc'];?></span>
                                </p>
                            </div>
                <?php
                        endforeach;
                    endif;
                ?>
                <div class="home-numbers--item">
                    <p>
                        <?php echo $numbers['description']; ?>
                    </p>
                    <a href="<?php echo $numbers['link']; ?>" class="btn-more">
                        saiba mais
                    </a>
                </div>
                <small class="alert">
                    <?php echo $numbers['alert']; ?>
                </small>
            </div>
        </section>
    <?php endif; ?>

    <section id="novidades" class="home-marcas" style="background-image: url(<?php bloginfo('template_url'); ?>/_assets/images/home-marcas.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-6">
                    <h6>NOSSAS MARCAS</h6>
                    <p>Junto com os mais modernos hospitais, clínicas, laboratórios e médicos reconhecidos vamos levar prevenção, diagnóstico, tratamento, cura para todos os cantos do Brasil.</p>
                    <div class="home-marcas--items">
                        <a href="<?php bloginfo('url'); ?>/marcas-do-portfolio">
                            <div class="row align-items-center justify-content-center align-content-center pb-0 pb-md-5">
                                <div class="col-md-4"><a href="https://www.humanasaude.com.br/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/home-marca-hospitalsantamaria.png" alt=""></a></div>
                                <div class="col-md-4"><a href="http://medimagem.com.br/saopedro" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/home-marca-hospitalsaopedro-alt.png" alt=""></a></div>
                                <div class="col-md-4"><a href="http://medimagem.com.br/hospitalvitoria" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/home-marca-hospitalvitoria-alt.png" alt=""></a></div>
                            </div>
                            
                            <div class="row align-items-center justify-content-center align-content-center pt-0 pb-0 pt-md-5 pb-md-5">
                                <div class="col-md-4"><a href="https://www.humanasaude.com.br/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/home-marca-humana-alt.png" alt=""></a></div>
                                <div class="col-md-4"><a href="http://imagemplena.com.br/home/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/home-marca-imagemplena-alt.png" alt=""></a></div>
                                <div class="col-md-4"><a href="https://www.medplan.com.br/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/home-marca-medplan-alt.png" alt=""></a></div>
                            </div>

                            <div class="row align-items-center justify-content-center align-content-center pt-0 pb-0 pt-md-5 pb-md-5">
                                <div class="col-md-4"><a href="https://medimagem.com.br/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/home-marca-medimagem-alt.png" alt=""></a></div>
                                <div class="col-md-4"><a href="http://www.oncomedica.com.br/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/home-marca-oncomedica-alt.png" alt=""></a></div>
                                <div class="col-md-4"><a href="https://medimagem.com.br/prontomed-adulto" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/home-marca-prontomed-alt.png" alt=""></a></div>
                            </div>
                            
                            <div class="row align-items-center justify-content-center align-content-center pt-0 pb-0 pt-md-5 pb-md-5">
                                <div class="col-md-4"><a href="https://medimagem.com.br/category/prontomed-infantil" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/home-marca-prontomedinfantil-alt.png" alt=""></a></div>
                                <div class="col-md-4"><a href="https://www.samp.com.br/es/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/home-marca-samp-alt.png" alt=""></a></div>
                                <div class="col-md-4"><a href="http://www.vitoriaaparthospital.com.br/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/_assets/images/home-marca-vitoriaapart-alt.png" alt=""></a></div>
                            </div>
                        </a>
                    </div>
                    
                    <div class="text-right">
                        <a href="<?php bloginfo('url'); ?>/marcas-do-portfolio" class="btn-more">
                            saiba mais
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php get_template_part('section','noticias'); ?>

<?php get_footer(); ?>