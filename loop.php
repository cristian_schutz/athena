<?php
    // Our include
    define('WP_USE_THEMES', false);
    require_once('../../../wp-load.php');
    
    // Our variables
    $numPosts = (isset($_GET['numPosts'])) ? $_GET['numPosts'] : 0;
    $page = (isset($_GET['pageNumber'])) ? $_GET['pageNumber'] : 0;
    
    $the_query = new WP_Query(array(
        'posts_per_page' => $numPosts,
        'paged'          => $page
    ));
    
    if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
?>
    <article class="grid-item">
        <div class="main-noticias-item <?php echo !(has_post_thumbnail()) ? 'noimg' : ''; ?>">
            <a href="<?php the_permalink(); ?>">
                <?php
                    if(has_post_thumbnail()){
                        echo '<div class="main-noticias-item--img">';
                            the_post_thumbnail('medium');
                        echo '</div>';
                    }
                ?>
                <div class="main-noticias-item--text content">
                    <span><?php the_title(); ?></span>
                </div>
            </a>
        </div>
    </article>
<?php
        }                   
        wp_reset_postdata();
    }
?>