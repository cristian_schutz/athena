<?php /* Template Name: Fale Conosco */ ?>
<?php get_header(); the_post(); ?>
<section class="fale-conosco">
	<div class="container">
		<div class="fale-conosco--highlight">
			<h6>Atenção<br class="hidden-sm hidden-xs"> e cuidado<br class="hidden-sm hidden-xs"> sempre.</h6>
		</div>
		<div class="fale-conosco--text">
			<p>
				ELDORADO BUSINESS TOWER<br>
				Av. Nações Unidas 8501, 4º andar <br>
				Pinheiros - São Paulo - SP<br>
				05425-070
			</p>
		</div>
		<div class="fale-conosco--form">
			<form action="" class="js-form-fale-conosco">
				<div class="form--inputs">
					<div class="post-content">
						<p>Deixe aqui seus dados, comentários, dúvidas e sugestões que entraremos em contato.</p>
					</div>
					<input type="hidden" name="action" value="method_faleconosco">
					<input type="hidden" name="assunto" value="Fale Conosco">
					<div class="form-group">
						<label for="nome">Nome</label>
						<input type="text" class="form-control" id="nome" name="nome" aria-describedby="emailHelp" required>
					</div>
					<div class="form-group">
						<label for="email">E-mail</label>
						<input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" required>
					</div>
					<div class="form-group">
						<label for="telefone">Telefone</label>
						<input type="text" class="form-control js-mask-phone" id="telefone" name="telefone" aria-describedby="emailHelp" required>
					</div>
					<div class="form-group">
						<label for="msg">Mensagem</label>
						<textarea class="form-control" id="msg" name="mensagem" rows="3" required></textarea>
					</div>
					<button><i>>></i> enviar</button>
				</div>
				<div class="form--return"></div>
			</form>
		</div>
	</div>
</section>
<?php get_footer(); ?>