    <section class="main-noticias">
        <div class="container">
            <div class="main-noticias--heading">
                <h4>
                    NOTÍCIAS
                    <a href="<?php bloginfo('url'); ?>/noticias" class="btn-more">
                        saiba mais
                    </a>
                </h4>
            </div>
            <div class="row">
                <?php
                    $args = array('post_type'=>'post','posts_per_page'=>'4');
                    $the_query = new WP_Query( $args );
                    if ( $the_query->have_posts() ) {
                        while ( $the_query->have_posts() ) {
                            $the_query->the_post();
                ?>
                    <article class="col-lg-3">
                        <div class="main-noticias-item <?php echo !(has_post_thumbnail()) ? 'noimg' : ''; ?>">
                            <a href="<?php the_permalink(); ?>">
                                <?php
                                    if(has_post_thumbnail()){
                                        echo '<div class="main-noticias-item--img">';
                                            the_post_thumbnail('medium');
                                        echo '</div>';
                                    }
                                ?>
                                <div class="main-noticias-item--text content">
                                    <span><?php the_title(); ?></span>
                                </div>
                            </a>
                        </div>
                    </article>
                <?php
                        }                   
                        wp_reset_postdata();
                    }
                ?>
            </div>
        </div>
    </section>