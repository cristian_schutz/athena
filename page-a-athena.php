<?php /* Template Name: A Athena */ ?>
<?php get_header(); ?>
    <section class="athena-header" style="background-image: url(<?php bloginfo('template_url'); ?>/_assets/images/athena-header.jpg)">
        <h4>Cuidando de sua<br> saúde em todas<br> as fases da vida.</h4>
    </section>

    <section class="athena-quemsomos">
        <div class="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 mt-qmsomos-item">
                        <div class="position-relative pt-0 pt-md-5 pb-0 pb-md-5 mb-0 mb-md-5">
                            <span class="athena-quemsomos--bg alt" style="background-color: #0974bd;"></span>
                            <div class="main-heading">
                                <h5 style="color: #fff;">Quem somos</h5>
                            </div>
                            <div class="post-content">
                                <p style="color: #fff;">A Athena já nasceu grande a partir da união de operadoras, hospitais e do fundo de investimento Pátria. Em praticamente 1 ano, saímos da ideia de organizar um novo modelo de negócio saudável e sustentável na área de saúde e nos tornamos, junto a parceiros de sucesso, uma das maiores empresas de saúde suplementar do país. </p>
                            </div>
                        </div>
                        
                        <h4 class="athena-quemsomos--highlight">
                            Juntos<br> 
                            vamos<br> 
                            mais<br> 
                            longe.
                        </h4>
                    
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-6">
                        <div class="position-relative mb-0 mb-md-5" style="min-height: 800px;">
                            <span class="athena-quemsomos--bg" style="background-image: url(<?php bloginfo('template_url'); ?>/_assets/images/athena-1.jpg)"></span>
                            <div class="post-content pt-0 pt-md-5">
                                <h5>E queremos dar passos<br class="d-none d-md-block"> cada vez maiores</h5>
                                <p>Nosso crescimento está baseado em um modelo de atuação que soma experiência e talentos de negócios bem-sucedidos, por todo o Brasil, e adiciona a eles competências de gestão, investimento em infra-estrutura, tecnologia e inovação. De forma sólida, segura e de comprovado sucesso.</p>
                            </div>
                        </div>
                        <div class="post-content mt-0 mt-md-5 pt-0 pt-md-5">
                            <p style="color:#737373;">A Athena promove uma visão de crescimento contínuo, com respeito, transparência e confiança total em suas relações. Além de oferecermos todo o suporte e incentivo, acreditamos na preservação do legado de cada empresa associada e valorização dos recursos humanos envolvidos. Como parceiros, damos as condições ideais para o foco na entrega de serviços de saúde de alta qualidade. </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="position-relative d-block" style="min-height: 650px;">
                            <span class="athena-quemsomos--bg alt" style="background-image: url(<?php bloginfo('template_url'); ?>/_assets/images/athena-2.jpg)"></span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="position-relative  mt-0 mt-md-5 pt-0 pt-md-5" style="min-height: 629px;">
                            <span class="athena-quemsomos--bg" style="background: #f1f1ee;)"></span>
                            <div class="post-content">
                                <ul>
                                    <li>Gestão responsável e comprometida com a atenção e cuidado aos pacientes</li>
                                    <li>Respeito ao legado das empresas investidas do grupo</li>
                                    <li>Promoção e valorização das competências construídas pelas empresas associadas contando com o grupo Athena Saúde como parceira da gestão</li>
                                    <li>Inteligência, tecnologia, dados e processos que refletem diretamente na qualidade e agilidade de atendimento aos pacientes</li>
                                    <li>Gestão em rede própria de saúde otimizando recursos e mantendo a excelência de qualidade ao paciente</li>
                                </ul>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </section>

    <section class="athena-numeros">
        <div class="container d-block d-md-flex">
            <?php 
                while( have_rows('numbers') ): the_row();
                    echo '<p>'.get_sub_field('numbers').'<strong>'.get_sub_field('number').'</strong>'.get_sub_field('text').'</p>';
                endwhile;
            ?>
        </div>
    </section>

    <section class="athena-mapa" style="background-color: #c7d3dc;">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="post-content">
                        <h2>Um Brasil de oportunidades</h2>
                        <p>A Athena mapeia continuamente oportunidades de crescimento por todo o Brasil. Operadoras, clínicas, hospitais e laboratórios sólidos, com uma história reconhecida, que queiram fazer parte desse modelo integrado e pioneiro. Onde você estiver, a Athena também pode estar. Juntos, vamos mais longe e com mais velocidade.</p>
                    </div>
                    <div class="row mt-md-5 pt-2 pt-md-5">
                        <div class="col-md-6 col-12 post-content">
                            <h6>
                                <img src="<?php bloginfo('template_url'); ?>/_assets/images/athena-pin.png" alt="">
                                Espírito Santo
                            </h6>
                            <p>
                                Samp<br>Vitória Apart Hospital
                            </p>
                        </div>
                        <div class="col-md-6 col-12 post-content">
                            <h6>
                                <img src="<?php bloginfo('template_url'); ?>/_assets/images/athena-pin.png" alt="">
                                Piauí
                            </h6>
                            <p>
                                Hospital Santa Maria<br>
                                Hospital São Pedro<br>
                                Hospital Vitória de Timon<br>
                                Humana Saúde<br>
                                Imagem Plena<br>
                                Med Imagem<br>
                                Medplan<br>
                                Oncomédica<br>
                                Pronto Med Adulto<br>
                                Pronto Med Infantil
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <a href="<?php bloginfo('url'); ?>/marcas" class="btn-more">conheça nossas marcas</a>
        </div>
    </section>
<?php get_footer(); ?>