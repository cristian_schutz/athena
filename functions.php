<?php 

	add_theme_support( 'post-thumbnails' );

	add_filter('show_admin_bar', '__return_false');

	# elementor custom widgets
	include dirname(__FILE__) . "/inc/form-fale-conosco/fale-conosco.php";
	include dirname(__FILE__) . "/inc/form-newsletter/newsletter.php";

	function cc_mime_types($mimes) {
	       $mimes['svg'] = 'image/svg+xml';
	       return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');

	function theme_styles(){
	    wp_enqueue_style( 'theme_css', get_template_directory_uri() . '/_assets/css/app.css' );
	}
	add_action( 'wp_enqueue_scripts', 'theme_styles' ); 

	function theme_js(){
	    global $wp_scripts;
	    wp_register_script( 'html5_shiv', 'https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js', '', '', false );
		wp_register_script( 'respond_js', 'https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js', '', '', false );
	    $wp_scripts->add_data( 'html5_shiv', 'conditional', 'lt IE 9' );
		$wp_scripts->add_data( 'respond_js', 'conditional', 'lt IE 9' );
	    wp_enqueue_script( 'theme_js', get_template_directory_uri() . '/_assets/js/app.bundle.js', array('jquery'), '', true );
		wp_localize_script(
			'theme_js',
			'crossapi',
			array(
				'url'               => get_bloginfo('url'),
				'template_url'		=> get_bloginfo('template_url'),
				'ajaxurl'			=> admin_url( 'admin-ajax.php' )
			)
		);
	}
	add_action( 'wp_enqueue_scripts', 'theme_js' );

	// admin css
	function theme_admin() {
		wp_enqueue_style('admin_styles' , get_template_directory_uri() . '/_assets/css/admin.css');
	}
	add_action('elementor/editor/before_enqueue_styles', 'theme_admin');
	add_action('admin_head', 'theme_admin');

	function remove_admin_login_header() {
	    remove_action('wp_head', '_admin_bar_bump_cb');
	}
	add_action('get_header', 'remove_admin_login_header');

	function register_my_menu() {
	  register_nav_menu('header',__( 'Header' ));
	  register_nav_menu('footer',__( 'Footer' ));
	}
	add_action( 'init', 'register_my_menu' );