		<footer class="footer">
			<a href="<?php bloginfo('url'); ?>" class="logo">
				<img src="<?php bloginfo('template_url') ?>/_assets/images/logo.png" alt="Logo Athena">
			</a>
			<div class="footer--slogan">
				<div class="container">
					Cuidado, inovação e saúde de qualidade, de Norte a Sul do Brasil.
					<a href="#" class="js-scrolltop">
						<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-angle-up fa-w-10"><path fill="currentColor" d="M177 159.7l136 136c9.4 9.4 9.4 24.6 0 33.9l-22.6 22.6c-9.4 9.4-24.6 9.4-33.9 0L160 255.9l-96.4 96.4c-9.4 9.4-24.6 9.4-33.9 0L7 329.7c-9.4-9.4-9.4-24.6 0-33.9l136-136c9.4-9.5 24.6-9.5 34-.1z" class=""></path></svg>
					</a>
				</div>
			</div>
			<nav>
				<a href="<?php bloginfo('url'); ?>/termos-de-uso">Termos de uso</a>
				<a href="<?php bloginfo('url'); ?>/politica-de-privacidade">Política de privacidade</a>
			</nav>
		</footer>
	</div>
	<?php wp_footer(); ?>
</body>
</html>