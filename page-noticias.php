<?php /* Template Name: Notícias */ ?>
<?php get_header(); ?>
    <section class="noticias">
        <header class="noticias-header" style="background-image: url(<?php bloginfo('template_url'); ?>/_assets/images/noticias.jpg);">
            <div class="container">
                <div class="noticias-header--highlight">
                    <h6 style="color: #fff;">Viver bem <br>com Athena.</h6>
                </div>
                <div class="noticias-header--text">
                    <p>Juntos crescemos com qualidade.</p>
                </div>
            </div>
        </header>
        <div class="noticias-loop">
            <div class="container">
                <div id="ajax-content" class="grid"></div>
                <div class="text-center">
                    <a href="#" class="noticias-btn"> <i>+</i> Carregar</a>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>