<?php /* Template Name: Termos de uso */ ?>

<?php get_header(); the_post(); ?>
<section class="termos">
	<div class="termos-header" style="background-image: url(<?php bloginfo('template_url'); ?>/_assets/images/termos.jpg);"></div>

	<div class="container">
        <div class="row">
            <div class="col-md-10 offset-1">
                <div class="termos-heading">
                    <h2><?php the_title(); ?></h2>
                </div>

                <div class="post-content">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
	</div>
</section>
<?php get_footer(); ?>