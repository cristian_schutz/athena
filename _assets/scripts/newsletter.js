jQuery(document).ready(function($) {
   
	$('form.js-form-newsletter').validate({
		errorPlacement: function(error, element) {
			error.insertAfter('label[for='+element.attr('id')+']')
		},
		submitHandler: function (form) {
			var button = $(form).find('button'),
			    currentform = $(form),
				button_label = button.html(),
				fields = $(form).serializeArray();

			button.attr('disabled');
			button.attr('type', 'button');
			button.find('svg').addClass('d-none');
			button.append('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');

			$.ajax({
				url: crossapi.ajaxurl,
				type: 'POST',
				data: $(form).serialize(),
			}).done(function(response) {

				var data = JSON.parse(response);
				
				$(form).find(".form--return").addClass('alert '+data.class).html(data.messenge);

				button.removeAttr('disabled');
				button.attr('type', 'submit');
				button.html(button_label);
				button.find('i').remove();
				button.find('svg').addClass('d-block');

				if (data.status == "success") {
					if (fields[0].name == "redirect_url") {
						window.location.href = fields[0].value;
					} else {
	                    $(form).find(".form--inputs").addClass('hidden');
						$(form).find(".form--inputs").slideUp();	
					}
				} else {
					$(form).find(".form--return").slideDown();
				}
			});

			return false;
		}
	});	

});
