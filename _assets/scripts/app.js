const css = require('../styles/app.scss');

import 'bootstrap';
import 'popper.js';
import 'owl.carousel';
import 'jquery-match-height';
import 'jquery-mask-plugin/dist/jquery.mask.js';
import 'jquery-validation/dist/jquery.validate.js';
import './fale-conosco.js';
import './loop.js';
   
window.onload = function() {
    $('.js-toggle-header-search').click(function(event) {
        $('.header-search').toggleClass('active');
    });

    $('.header-mobile--button').on('click', function(){
        $(this).toggleClass('active')
        $('.header-mobile-nav').toggleClass('active');
        if($('.header-mobile--button').hasClass('active')){
            $('.header-mobile--button span').html('FECHAR');
        }else{
            $('.header-mobile--button span').html('MENU');    
        }
    })

    $('.js-scrollto').on('click', function(e){
        var hash = $(this).attr('href')
        var offset = 0
        $('html,body').animate({
          scrollTop: $(hash).offset().top - offset
        }, 400);        
        return false;
    })

    $('.js-scrolltop').on('click', function(e){
        $('html,body').animate({
          scrollTop: 0
        }, 800);
        return false;
    })

    var slider = $(".home-slider .owl-carousel");
    if(slider.length > 0) {
        slider.owlCarousel({
            items: 1,
            autoplay: true,
            dots: false,
            loop: true,
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
        });
    }

} //window on load