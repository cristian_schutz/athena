jQuery(document).ready(function($) {
    var page = 1;
    var loading = true;
    var $window = $(window);
    var $content = $("#ajax-content");
    var load_posts = function(){
        $.ajax({
            type       : "GET",
            data       : {numPosts : 8, pageNumber: page},
            dataType   : "html",
            url        : crossapi.template_url+"/loop.php",
            beforeSend : function(){
                if(page != 1){
                    $('.noticias-btn').addClass('loading');
                }
            },
            success : function(data){
                $data = $(data);
                if($data.length){
                    $data.hide();
                    $content.append($data);
                    $data.fadeIn(500, function(){
                        $('.noticias-btn').removeClass('loading');
                        loading = false;
                    });
                } else {
                    $('.noticias-btn').removeClass('loading');
                }
            },
            error     : function(jqXHR, textStatus, errorThrown) {
                $('.noticias-btn').removeClass('loading');
                alert(jqXHR + " :: " + textStatus + " :: " + errorThrown);
            }
        });
    }
    load_posts();
    $('.noticias-btn').on('click', function() {
        var content_offset = $content.offset();
        console.log('este');
        console.log(content_offset.top);
        if(!loading && ($window.scrollTop() + $window.height()) > ($content.scrollTop() + $content.height() + content_offset.top)) {
            loading = true;
            page++;
            load_posts();
        }
        return false;
    });
});