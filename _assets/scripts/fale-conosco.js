jQuery(document).ready(function($) {
    $('.js-mask-date').mask('00/00/0000');
    $('.js-mask-time').mask('00:00:00');
    $('.js-mask-datetime').mask('00/00/0000 00:00:00');
    $('.js-mask-cep').mask('00000-000');
    $('.js-mask-cpf').mask('000.000.000-00', {reverse: true});
    $('.js-mask-money').mask('000.000.000.000.000,00', {reverse: true});
    var maskBehavior = function (val) {
     return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    options = {onKeyPress: function(val, e, field, options) {
     field.mask(maskBehavior.apply({}, arguments), options);
     }
    };
    $('.js-mask-phone').mask(maskBehavior, options);

	$('form.js-form-fale-conosco').validate({
		errorPlacement: function(error, element) {
			error.insertAfter('label[for='+element.attr('id')+']')
		},
		submitHandler: function (form) {
			var button = $(form).find('button'),
			    currentform = $(form),
				button_label = button.html(),
				fields = $(form).serializeArray();

			button.attr('disabled');
			button.attr('type', 'button');
			button.append('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');

			$.ajax({
				url: crossapi.ajaxurl,
				type: 'POST',
				data: $(form).serialize(),
			}).done(function(response) {

				var data = JSON.parse(response);
				
				$(form).find(".form--return").addClass('alert '+data.class).html(data.messenge);

				button.removeAttr('disabled');
				button.attr('type', 'submit');
				button.html(button_label);

				if (data.status == "success") {
					if (fields[0].name == "redirect_url") {
						window.location.href = fields[0].value;
					} else {
	                    $(form).find(".form--inputs").addClass('hidden');
						$(form).find(".form--inputs").slideUp();	
					}
				} else {
					$(form).find(".form--return").slideDown();
				}
			});

			return false;
		}
	});	

});
