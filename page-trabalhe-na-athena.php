<?php /* Template Name: Fale Conosco */ ?>
<?php get_header(); the_post(); ?>
<section class="fale-conosco">
	<div class="container">
		<div class="fale-conosco--highlight">
			<h6>Atenção<br class="hidden-sm hidden-xs"> e cuidado<br class="hidden-sm hidden-xs"> sempre.</h6>
		</div>
		<div class="fale-conosco--form">
			<form action="" class="js-form-fale-conosco">
				<div class="form--inputs">
					<div class="post-content">
						<p><strong>Nossa gente</strong><br>
						Somos mais de 5.000 pessoas que compartilha do mesmo propósito que é contribuir para a qualidade dos serviços de saúde no Brasil.</p>
						<p><strong>Perfil Athena</strong><br>
						Conduzimos nossos negócios com ética e profissionalismo, valorizando a qualidade,  trabalho em equipe e um ambiente colaborativo baseado em meritocracia.</p>
						<p><strong>Onde estamos</strong><br>
						Temos as operações de saúde distribuídas em diversas regiões do país com nossa sede corporativa localizada no Eldorado Business Tower em São Paulo Capital.</p>
					</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>