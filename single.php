<?php get_header(); the_post(); ?>
<section class="single-noticias">
	<header class="noticias-header" style="background-image: url(<?php bloginfo('template_url'); ?>/_assets/images/noticias.jpg);">
		<div class="container">
			<div class="noticias-header--highlight">
				<h6 style="color: #fff;">Viver bem <br>é assim.</h6>
			</div>
			<div class="noticias-header--text">
				<p>Juntos crescemos com qualidade.</p>
			</div>
		</div>
	</header>

	<div class="container">
		<div class="row">
			<div class="col-lg-8">
				<div class="single-noticias--header">
					<h2><?php the_title(); ?></h2>
					
					<time>
						<?php the_date('d/m/Y'); ?>
					</time>
				</div>

				<div class="post-content">
					<?php the_content(); ?>
				</div>
			</div>
			
			<div class="col-lg-3 offset-lg-1">
				<aside>
					<?php
						$args = array(
							'post_type'      => 'post',
							'posts_per_page' => '2',
							'post__not_in'=>array(get_the_ID())
						);
						$the_query = new WP_Query( $args );
						if ( $the_query->have_posts() ) {
							while ( $the_query->have_posts() ) {
								$the_query->the_post();
					?>
						<article>
							<div class="main-noticias-item <?php echo !(has_post_thumbnail()) ? 'noimg' : ''; ?>">
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail('medium'); ?>
									<div class="main-noticias-item--text content">
										<span><?php the_title(); ?></span>
									</div>
								</a>
							</div>
						</article>
					<?php
							}                   
							wp_reset_postdata();
						}
					?>
					<div class="text-center">
						<a href="<?php bloginfo('url'); ?>/noticias" class="btn-more">
							veja mais
						</a>
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>